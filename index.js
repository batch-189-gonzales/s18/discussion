// Function Parameters - variables that wait for a value from an arguement in the function invocation
function sayMyName(name) {
	console.log('My name is ' + name);
};

sayMyName('Listher Gonzales');

// You can reuse functions by invoking them at any time or any part of your code as long as you have declared them first
sayMyName('Jett Revive');
sayMyName('Annie Areyouok');

let myName = 'Beelze Bob';
sayMyName(myName);

// You can use arguments and parameters to make the data inside your function dynamic
function sumOfTwoNumbers(firstNumber, secondNumber) {
	let sum = 0;

	sum = firstNumber + secondNumber;
	console.log('The sum of ' + firstNumber + ' and ' + secondNumber + ' is ' + sum);
};

sumOfTwoNumbers(10, 15);
sumOfTwoNumbers(99, 1);
sumOfTwoNumbers(50, 20);

// You can pass a function as an argument for another function
function argumentFunction() {
	console.log('This is a function that was passed as an argument.');
};

function parameterFunction(argumentFunction) {
	argumentFunction();
}

parameterFunction(argumentFunction);

// Real World Application

/*
	Imagine a product page where you have an 'add to cart' button wherein you have the button element itself displayed on the page BUT you don't have the functionality of it yet. This is where passing a function as an argument comes in in order for you to ass or have access to a function to add functionality to your button once it is clicked
*/

let addToCartBtn = document.querySelector('#add-to-cart-btn');

function addProductToCart() {
	console.log('Click me to add to cart');
};

addToCartBtn.addEventListener('click', addProductToCart);

// Extra arguments will be ignored while lack of argument will pass undefined value
function displayFullName(firstName, middleInitial, lastName) {
	console.log('Your full name is: ' + firstName + ' ' + middleInitial + '. ' + lastName);
};

displayFullName('Johnny', 'Depp');
displayFullName('Johnny', 'B', 'Good', 'Yeah');

// STRING INTERPOLATION - you can interpolate the variables using back-tick (``) instead of regular concatenation
function displayMovieDetails(title, synopsis, director){
	console.log(`The movie is titled ${title}`);
	console.log(`The synopsis is ${synopsis}`);
	console.log(`The movie director is ${director}`);
};

displayMovieDetails('Sisterakas', 'no synopsis', 'Wenn Deramas');

// RETURN STATEMENT - return statement should be the last statement within a function
function displayPlayerDetails(name, age, playerClass) {
	// console.log(`Player name: ${name}`);
	// console.log(`Player age: ${age}`);
	// console.log(`Player class: ${playerClass}`);
	let playerDetails = `Name: ${name}, Age: ${age}, Class: ${playerClass}`;

	return playerDetails;

};

console.log(displayPlayerDetails('Elsworth', 120, 'Paladin'));